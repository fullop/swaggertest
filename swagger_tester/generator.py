'''
Swagger to JSON
Dependent on OpenAPI 2
Not compatible with OpenAPI 3.
'''
from abc import ABC
from datetime import datetime
from math import ceil
from random import random, randrange
import yaml
import pathlib
from typing import Tuple, Dict

from xeger import Xeger


def openFileFromCurrentDir(filename: str, mod: str = 'r'):
    """
    :param filename: 읽어올 파일 명
    :param mod: 읽기쓰기모드
    :return: 읽은 파일 객체
    """
    return open('{}/{}'.format(str(pathlib.Path(__file__).parent), filename), mod)


with openFileFromCurrentDir('SWAGGER.yml') as yml:
    SWAGGER = yaml.safe_load(yml)

swagger_file = None


def loadSwaggerJSON(swagger: dict) -> None:
    """
    :param swagger: 읽어온 swagger를 dict로 변환한 것
    :return: None
    """
    global swagger_file
    swagger_file = swagger


class SwaggerComponent(ABC):
    """
    Swagger에서 사용하는 인자에 대응되는 클래스

    get()을 이용해 읽어온 데이터와 동일한 형태의 JSON 객체를 생성
    """
    def get(self):
        """
        컴포넌트에 대응되는 JSON을 반환

        SwaggerComponent의 get은 접근 불가
        """
        raise NotImplementedError


class SwaggerObject(SwaggerComponent):
    """
    Swagger에서 object에 대응되는 클래스

    Python에서는 dict에 해당

    """
    def __init__(self, swagger_object: dict):
        self.__properties = dict()
        properties = None
        if SWAGGER['OBJECT']['PROPERTIES'] in swagger_object.keys():
            properties = swagger_object[SWAGGER['OBJECT']['PROPERTIES']].items()
        elif SWAGGER['OBJECT']['ADDITIONAL_PROPERTIES'] in swagger_object.keys():
            properties = swagger_object[SWAGGER['OBJECT']['ADDITIONAL_PROPERTIES']].items()
        for name, component in properties:
            self.__properties[name] = generateComponent(component)

    def get(self):
        return self.getExample()

    def getExample(self):
        json_object = dict()
        if len(self.__properties) > 0:
            for name, component in self.__properties.items():
                json_object[name] = component.get()
        return json_object


class SwaggerArray(SwaggerComponent):
    """
    Swagger에서 array에 대응되는 클래스

    Python에서는 list에 해당

    """
    def __init__(self, swagger_array: dict):
        self.__item_schema = generateComponent(swagger_array[SWAGGER['ARRAY']['ITEMS']])
        self.__min = swagger_array[SWAGGER['ARRAY']['MIN']] \
            if SWAGGER['ARRAY']['MIN'] in swagger_array.keys() else -1
        self.__max = swagger_array[SWAGGER['ARRAY']['MAX']] \
            if SWAGGER['ARRAY']['MAX'] in swagger_array.keys() else -1
        self.__uniqueItems = swagger_array[SWAGGER['ARRAY']['UNIQUEITEMS']] \
            if SWAGGER['ARRAY']['UNIQUEITEMS'] in swagger_array.keys() else None

    def get(self):
        return self.getThreeItemInList()

    def getThreeItemInList(self):
        json_array = list()
        for _ in range(randrange(1, 4)):
            json_array.append(self.__item_schema.get())
        return json_array


class SwaggerString(SwaggerComponent):
    def __init__(self, swagger_string: dict):
        self.__min = swagger_string[SWAGGER['STRING']['MIN']] \
            if SWAGGER['STRING']['MIN'] in swagger_string.keys() else -1
        self.__max = swagger_string[SWAGGER['STRING']['MAX']] \
            if SWAGGER['STRING']['MAX'] in swagger_string.keys() else -1
        self.__pattern = swagger_string[SWAGGER['STRING']['PATTERN']] \
            if SWAGGER['STRING']['PATTERN'] in swagger_string.keys() else None
        self.__example = swagger_string[SWAGGER['STRING']['EXAMPLE']] \
            if SWAGGER['STRING']['EXAMPLE'] in swagger_string.keys() else None
        self.__enum = swagger_string[SWAGGER['STRING']['ENUM']] \
            if SWAGGER['STRING']['ENUM'] in swagger_string.keys() else None
        self.__format = swagger_string[SWAGGER['ATTRIBUTE']['FORMAT']] \
            if SWAGGER['ATTRIBUTE']['FORMAT'] in swagger_string.keys() else None

    def get(self):
        if self.__enum is not None:
            return self.getEnum()
        elif self.__format is not None:
            return self.getFormat()
        elif self.__example is not None:
            return self.getExample()
        else:
            return self.getRandom()

    def getExample(self):
        # can apply attribute
        json_string = self.__example
        return json_string

    def getEnum(self):
        json_string = self.__enum[randrange(0, len(self.__enum))]
        return json_string

    def getRandom(self):
        x = Xeger()
        json_string = x.xeger('[a-zA-Z]{10}')
        return json_string

    def getFormat(self):
        if SWAGGER['FORMAT']['STRING']['DATETIME'] == self.__format:
            return "{:%Y-%m-%dT%H:%M:%S.}".format(datetime.now())+"{:%f}".format(datetime.now())[:3]+'Z'
        else:
            raise NotImplementedError


class SwaggerInteger(SwaggerComponent):
    def __init__(self, swagger_integer: dict):
        self.__min = swagger_integer[SWAGGER['NUMERIC']['MIN']] \
            if SWAGGER['NUMERIC']['MIN'] in swagger_integer.keys() else None
        self.__max = swagger_integer[SWAGGER['NUMERIC']['MAX']] \
            if SWAGGER['NUMERIC']['MAX'] in swagger_integer.keys() else None
        self.__format = swagger_integer[SWAGGER['ATTRIBUTE']['FORMAT']] \
            if SWAGGER['ATTRIBUTE']['FORMAT'] in swagger_integer.keys() else SWAGGER['FORMAT']['INTEGER']['INT32']

    def get(self):
        if self.__min is not None or self.__max is not None:
            return self.getValueInRange()
        else:
            return self.getRandomValueWithoutRange()

    def getRandomValueWithoutRange(self):
        # can apply attribute
        json_integer = randrange(-10000, 10000, 1)
        return json_integer

    def getValueInRange(self):
        # can apply attribute
        min_range = self.__min if self.__min is not None else -1000
        max_range = self.__max if self.__max is not None else 1000
        json_integer = randrange(min_range, max_range, 1)
        return json_integer


class SwaggerNumber(SwaggerComponent):
    def __init__(self, swagger_number: dict):
        self.__min = swagger_number[SWAGGER['NUMERIC']['MIN']] \
            if SWAGGER['NUMERIC']['MIN'] in swagger_number.keys() else None
        self.__max = swagger_number[SWAGGER['NUMERIC']['MAX']] \
            if SWAGGER['NUMERIC']['MAX'] in swagger_number.keys() else None
        self.__format = swagger_number[SWAGGER['ATTRIBUTE']['FORMAT']] \
            if SWAGGER['ATTRIBUTE']['FORMAT'] in swagger_number.keys() else SWAGGER['FORMAT']['NUMBER']['FLOAT']

    def get(self):
        return self.getRandom()

    def getRandom(self):
        # can apply attribute
        json_number = random() * 10000 - 5000
        return json_number


class SwaggerBoolean(SwaggerComponent):
    def get(self):
        # can apply attribute
        json_boolean = True if ceil(random() * 10000) % 2 == 0 else False
        return json_boolean


class SwaggerFile(SwaggerComponent):
    def get(self):
        # can apply attribute
        json_file = openFileFromCurrentDir('files/test.txt', 'rb')
        return json_file


class SwaggerComponents:
    """
    컴포넌트 집합

    get에서 집합 내부의 컴포넌트를 get한 결과를 dict에 넣어서 반환
    """
    def __init__(self):
        self.__components = dict()

    def add(self, swagger_name: str, swagger_components: SwaggerComponent):
        self.__components[swagger_name] = swagger_components

    def get(self):
        json_dict = dict()
        for name, component in self.__components.items():
            json_dict[name] = component.get()
        return json_dict


class SwaggerQueries(SwaggerComponents):
    def get(self):
        queries = SwaggerComponents.get(self)
        return queriesToQueryString(queries)


class SwaggerBody(SwaggerComponents):
    def get(self):
        components = SwaggerComponents.get(self)
        if 'body' in components.keys():
            return components['body']
        else:
            return {}


def generateComponent(schema: dict) -> SwaggerComponent:
    if SWAGGER['SINGLE_VALUE']['TYPE'] in schema.keys():
        if SWAGGER['TYPE']['OBJECT'] == schema[SWAGGER['SINGLE_VALUE']['TYPE']]:
            return SwaggerObject(schema)
        elif SWAGGER['TYPE']['ARRAY'] == schema[SWAGGER['SINGLE_VALUE']['TYPE']]:
            return SwaggerArray(schema)
        elif SWAGGER['TYPE']['NUMBER'] == schema[SWAGGER['SINGLE_VALUE']['TYPE']]:
            return SwaggerNumber(schema)
        elif SWAGGER['TYPE']['INTEGER'] == schema[SWAGGER['SINGLE_VALUE']['TYPE']]:
            return SwaggerInteger(schema)
        elif SWAGGER['TYPE']['STRING'] == schema[SWAGGER['SINGLE_VALUE']['TYPE']]:
            return SwaggerString(schema)
        elif SWAGGER['TYPE']['BOOL'] == schema[SWAGGER['SINGLE_VALUE']['TYPE']]:
            return SwaggerBoolean()
        elif SWAGGER['TYPE']['FILE'] == schema[SWAGGER['SINGLE_VALUE']['TYPE']]:
            return SwaggerFile()
    if SWAGGER['SINGLE_VALUE']['REF'] in schema.keys():
        return findReference(schema)


def findReference(swagger_ref: dict) -> SwaggerComponent:
    definitions = swagger_ref[SWAGGER['SINGLE_VALUE']['REF']].split('/')[1:]
    definition = swagger_file
    for sub_def in definitions:
        definition = definition[sub_def]
    return generateComponent(definition)


def queriesToQueryString(json_queries: dict) -> str:
    query = ''
    for key, val in json_queries.items():
        if isinstance(val, list):
            for element in val:
                query = query + '{}={}&'.format(key, element)
        else:
            query = query + '{}={}&'.format(key, val)
    return query[:-1]


def generateRequestArgument(parameters: list) \
        -> Tuple[SwaggerComponents, SwaggerComponents, SwaggerComponents, SwaggerComponents, SwaggerComponents]:
    if swagger_file is None:
        raise Exception('You must load swagger file. should import loadSwaggerJSON and add Swagger file to Library')
    swagger_parameters = SwaggerComponents()
    swagger_data = SwaggerBody()  # body
    swagger_files = SwaggerComponents()  # formData
    swagger_queries = SwaggerQueries()  # url query
    swagger_header = SwaggerComponents()

    for parameter in parameters:
        param_in = parameter['in']
        param_name = parameter['name']
        if param_in == SWAGGER['IN']['PATH']:
            swagger_parameters.add(param_name, generateComponent(parameter))
        elif param_in == SWAGGER['IN']['BODY']:
            swagger_data.add(param_name, generateComponent(parameter['schema']))
        elif param_in == SWAGGER['IN']['FORMDATA']:
            swagger_files.add(param_name, generateComponent(parameter))
        elif param_in == SWAGGER['IN']['QUERY']:
            swagger_queries.add(param_name, generateComponent(parameter))
        elif param_in == SWAGGER['IN']['HEADER']:
            swagger_header.add(param_name, generateComponent(parameter))
        else:
            raise AttributeError()
    return swagger_parameters, swagger_data, swagger_files, swagger_queries, swagger_header
