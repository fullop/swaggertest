import requests.sessions

session = requests.session()


def request(method: str, path: str, data: str = None, params: dict = None, files: dict = None):
    content_type = ''
    if len(files) == 0:
        content_type = 'application/json'
    else:
        if 'file' in files.keys():
            content_type = 'multipart/form-data; boundary=----WebKitFormBoundaryp7MA4YWxkTrZu0gW'
        else:
            content_type = 'application/x-www-form-urlencoded'

    args = {
        'url': path,
        'json': data,
        'params': params,
        'headers': {
            'method': method,
            'authority': 'petstore.swagger.io',
            'origin': 'petstore.swagger.io',
            'user-agent': 'petstore tester',
            'accept': 'application/json',
            'Content-Type': content_type
        },
        'files': files
    }
    if method == 'get':
        return session.get(**args)
    elif method == 'post':
        return session.post(**args)
    elif method == 'put':
        return session.put(**args)
    elif method == 'delete':
        return session.delete(**args)
    else:
        raise AttributeError("Unexpected request method : {}".format(method))


def printPaths(paths: dict) -> None:
    for path in paths:
        print(path)
        for req_type, req_body in paths[path].items():
            print("    " + req_type)
            # if "200" not in req_body["responses"].keys():
            #     print("\t\t[{}]\n\t\t\tMSG : {}".format("200", "response sended data"))

            if len(req_body["parameters"]) > 0:
                print("\t\tPARAM :")
            for parameter in req_body["parameters"]:
                if 'type' not in parameter.keys():
                    param_type = 'object'
                else:
                    param_type = parameter['type']
                print("\t\t\t{} : {}".format(parameter['name'], param_type))

            print("\t\tResponse :")
            for status, res_body in req_body["responses"].items():
                print("\t\t\t[{}]".format(status))
                if 'schema' in res_body.keys():
                    print("\t\t\t\tMSG : {}\n\t\t\t\tBODY : {}".format(status, res_body['description'],
                                                                       res_body['schema']))
                else:
                    print("\t\t\t\tMSG : {}".format(status, res_body['description']))