import requests

if __name__ == "__main__":
    import os
    print(os.getcwd())

def test():
    args = {
        'url': 'http://localhost:8080/v2/pet',
        'data': '{  \"id\": 10,  \"category\": {    \"id\": 0,    \"name\": \"string\"  },  \"name\": \"doggie\",  \"photoUrls\": [    \"string\"  ],  \"tags\": [    {      \"id\": 0,      \"name\": \"string\"    }  ],  \"status\": \"available\"}',
        'headers': {
            'method': 'post',
            'user-agent': 'petstore',
            'accept': 'application/json',
            'Content-Type': 'application/json',
            'scheme': 'http'
        }
    }
    res = requests.post(url = args['url'], data = args['data'], headers = args['headers'])
    print(res.status_code)

    args = {
        'url': 'http://localhost:8080/v2/store/order',
        'headers': {
            'method': 'post',
            'user-agent': 'petstore',
            'accept': 'application/json',
            'Content-Type': 'application/json',
            'scheme': 'http'
        }
    }
    data = '{ "id": -4475, "petId": -4329, "quantity": 6877, "shipDate": "2021-01-27T12:42:26.566Z", "status": "delivered", "complete": "true" }'
    res = requests.post(url=args['url'], data=data, headers=args['headers'])
    print(res.status_code)
    print(str(res.content))