import json
from swagger_tester.translator import *
from swagger_tester.generator import loadSwaggerJSON, generateRequestArgument
from swagger_tester.request import request

# https://petstore3.swagger.io/api/v3/openapi.json
# https://petstore.swagger.io/v2/swagger.json

def testAll():
    from requests import get
    api = get("https://petstore.swagger.io/v2/swagger.json")
    swagger = json.loads(api.content)
    loadSwaggerJSON(swagger)

    basePath = 'http://127.0.0.1:8080/v2'
    paths = getRequestPaths(swagger)

    for requestPath, pathInfo in paths.items():
        print(requestPath)
        for method, requestInfo in pathInfo.items():
            (parameters, data, files, query, header) = generateArguments(requestInfo)
            requestPath = fillPath(requestPath, parameters)
            totalPath = basePath + requestPath + "?" + query
            response = request(method, totalPath, data, parameters, files)
            printRequestAndResponse(method, parameters, data, files, query, response)


def generateArguments(requestInfo: dict):
    params_prototype = getParameters(requestInfo)
    swagger_parameters, swagger_data, swagger_files, swagger_query, swagger_header \
        = generateRequestArgument(params_prototype)
    return swagger_parameters.get(), swagger_data.get(), swagger_files.get(), swagger_query.get(), swagger_header.get()


def printRequestAndResponse(method, parameters, data, files, query, response):
    if response.status_code == 200:
        return
    print("\t" + method)
    print("\t\tREQUEST:")
    if len(parameters) > 0:
        print("\t\t\tPARAM: " + toString(parameters))
    if len(query) > 0:
        print("\t\t\tQUERY: " + query)
    if len(data) > 0:
        if isinstance(data, list):
            print("\t\t\tDATA: [ {} ]".format(', '.join(str(val) for val in data)))
        else:
            print("\t\t\tDATA: " + toString(data))
    if len(files) > 0:
        print("\t\t\tFILE: " + toString(files))
    print("\t\tRESPONSE: " + str(response.status_code))
    print("\t\t\t\t" + str(response.content))


def toString(json_object: dict) -> str:
    return '{{ {} }}'.format(', '.join(': '.join((key, "'{}'".format(val) if isinstance(val,str) else str(val))) for (key, val) in json_object.items()))


if __name__ == "__main__":
    testAll()
